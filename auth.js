const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

// JSON Web Tokens

// Token Creation
module.exports.createAccessToken = (user) => {
	const data = {
		id : user._id,
		email : user.email,
		isAdmin : user.isAdmin
	}
	return jwt.sign(data, secret, {});
}

// Token Verification
module.exports.verify = (req, res, next) => {
	// The token is retrieved from the request header
	let token = req.headers.authorization;

	if(typeof token !== "undefined"){
		console.log(token)

		token = token.slice(7, token.length);
		console.log(token);

		// Validate the token using the "verify" method decrypting the token using the secret code
		return jwt.verify(token, secret, (err, data) => {
			// if JWT is not valid
			if(err){
				return res.send({auth : "failed"});
			}
			// if JWT is valid
			else{
				next();
			}
		})
	}
	// Token does not exist
	else{
		return res.send({auth : "failed"});
	}
}

// Token decryption
module.exports.decode = (token) => {
	// Token received and is no undefined
	if(typeof token !== "undefined"){

		// Retrives only the token and removes the "Bearer " prefix
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if (err){
				return null;
			}
			else
			{
				return jwt.decode(token, {complete:true}).payload
			}
		})
	}
	// Token does not exist
	else{
		return null;
	}
}
// Token does not exist
	// complete true will help us to retrieve additional information//JWS payload (set of claims): contains verifiable security statements, such as the identity of the user and the permissions they are allowed
	// jwt is a gift wrapping service that will secure the gift with a lock
// and when it reaches the user it will remove the lock
// next will allow to move to the next function
// Token received and is not undefined
// token ke sath hamesa bearer aata hai toh wo slice karna padhega