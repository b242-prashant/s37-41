// const express = require("express");
// const router = express.Router();
// const courseController = require("../controllers/course")
// const auth = require("../auth")
// //idhar se apan ne course controller ko import kiya

// router.post("/", auth.verify, (req, res) => {

// 	const data = {
// 		course: req.body,
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin
// 	}

// 	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));

// });


// //Allows us to export thr "router" object that will be accessed in our "index.js" file

// router.get("/all",(req,res) => {
// 	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
// })

// //all is called endpoint

// router.get("/",(req,res) => {
// 	courseController.getAllActive().then(resultFromController => res.send(
// 		resultFromController));
// })
// //specific course ka detail lane ke liye we need to add a parameter in the endpoint
// router.get("/:courseId", (req,res) =>{
// 	console.log(req.params.courseId);
// 	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
// })
// //Ab update krenge so update admin krega, so we will use jwt token
// router.put("/:courseId", auth.verify, (req,res) =>{
// 	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
// })

// //update krne ke liye put use krte hai and post new data banane ke liye

// module.exports = router;

// //on the first one, the whole payload is being generated (user id, isAdmin and email)

// //on the second one, only the isAdmin is being generated isAdmin: auth.decode(req.headers.authorization).isAdmin

// router.put("/:courseId/archive", auth.verify, (req,res) => {
// 	const data = {
// 		course: req.body,
// 		isAdmin: auth.decode(req.body.authorization).isAdmin
// 	}

// 	courseController.archiveCourse(req.params,data).then(resultFromController => res.send(resultFromController));
// })

const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course");
const auth = require("../auth");

// Route for creating a course
router.post("/", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));

});

// Route for retrieving all the courses
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})

// Route for retrieving all active courses
router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
})

// Route for retieving a specific course
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId);

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

// Route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})

// Route to archiving a course [s40 Activity ]
// A "PUT" request is used instead of "DELETE" request because of our approach in archiving and hiding the courses from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases
router.put("/:courseId/archive", auth.verify, (req, res) => {

	courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController));
	
});




// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;
//doubts:
// Is writing auth.verify in router.put or get or post enough for the authentication of the user as we did in route to update a course?