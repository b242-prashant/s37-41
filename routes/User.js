// const express = require("express");
// const router = express.Router();
// const userController = require("../controllers/User.js");
// const auth = require("../auth")
// router.post("/checkEmail",(req,res) => {
// 	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
// })
// module.exports = router;

// router.post("/register", (req,res) =>{
// 	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));

// })

// router.post("/login", (req,res) => {
// 	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));

// })

// router.get("/details", auth.verify,  (req,res) => {
// 	//we will now use the decode method to decode the token
// 	const userData = auth.decode(req.headers.authorization);

// 	userController.getProfile(req.body).then(resultFromController => res.send(resultFromController));
// })


// router.post("/enroll" /*auth.verify*/, (req,res) =>{
// 	let data = {
// 		// userId : auth.decode(req.headers.authorization)._id,
// 		courseId : req.body.courseId
// 		// we will enroll the  a particular user by taking its id and and adding it to a particular course by its id

// 	}
// 	userController.enroll(data).then(resultFromController => res.send(resultFromController));
// })
// // route to enroll user to a course


const express = require("express");
const router = express.Router();
const userController = require("../controllers/User");
const auth = require("../auth");

// Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for user authentication(login)
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for retrieving user details [s38 Activity]
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	// This means that we are passing an object to the getprofile which contains userId as a data
	userController.getProfile({userId : req.body.id}).then(resultFromController => res.send(resultFromController));

});

// Route to enroll user to a course
router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		courseId : req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController));
})

// router.post("/enrollWithAuth", auth.verify, (req, res) => {
// 	let data = {
// 		userId : auth.decode(req.headers.authorization)._id,
// 		courseId : req.body.courseId
// 	}

// 	userController.enrollWithAuth(data).then(resultFromController => res.send(resultFromController));
// })

// router.get("/userdetails", uth.verifya, (req, res) => {

// 	const userData = auth.decode(req.headers.authorization);

// 	// Provides the user's ID for the getProfile controller method
// 	// This means that we are passing an object to the getprofile which contains userId as a data
// 	userController.getUserDetails(userData).then(resultFromController => res.send(resultFromController));

// });


// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;